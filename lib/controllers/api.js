'use strict';
var mongoose = require('mongoose'),
	Pronostic = mongoose.model('Pronostic'),
	Country = mongoose.model('Country'),
	User = mongoose.model('User'),
	_ = require('underscore'),
	config = require('../config/config'),
    Thing = mongoose.model('Thing');



var Countries = function() {};

Countries.prototype.get = function(req, res) {
	Country.find(function(err, countries){
		if (err) { return res.send(err); }
		else { return res.json(countries); }
	});
};

/**
 * Country API
 */
exports.countries = new Countries();

/**
 * Get PronosticConf
 */
exports.queryPronostic = function(req,res) {
	return Pronostic.find(function(err, confs){
		if (!err) {
			return res.json(confs);
		} else {
			return res.send(err);
		}
	});
};

// Add a new pronostic conf
exports.savePronostic = function(req, res) {

	var id = req.body._id;
	var update = _.omit(req.body, '_id');
	Pronostic.findByIdAndUpdate(id,
		update,
		{w: 1},
		function(err, result){
			if (err) return res.json(400, err);
			res.send(result);
		});
	// newPronosticConf.save(function(err, proConf){
	// 	if (err) return res.json(400, err);

	// 	res.send(proConf);
	// });

};



exports.processScore = function(req, res) {

	// Get pronostic conf
	var prConf;

	var allUsers;
	User.find({}, function(err, users){
		allUsers = users;
		Pronostic.findOne({}, function(err, pronostic){
			prConf = pronostic;

			var luckyDude = function(match, mIndex, contextId) {
				var result;
				// Returns true if only 20% found winner for this match


				
				var usersFoundMatch = _.chain(allUsers)
				.filter(function(user){
					try {
						var matchPtr = user.pronostics[contextId].matches[mIndex];
					} catch (e) {
						return false;
					}

					return winnerFound(matchPtr, mIndex, contextId);
				})
				.value()

				 // Return true if less than 20% of users found this match
				 // console.log(usersFoundMatch.length + ' / ' + allUsers.length)
				 return (usersFoundMatch.length / allUsers.length * 100) < 20
				 // return true
			};

			var postPool = function() {
				return prConf.currentContext !== 'pools';
			};

			var winnerFound = function(match, mIndex, contextId) {
				var matchWinner;
				var matchPtr = prConf[contextId].matches[mIndex];
				if (!_.isUndefined(matchPtr)) {
					if (matchPtr.score1 > matchPtr.score2) {
						matchWinner = match.team1;
					} else if (matchPtr.score1 < matchPtr.score2) {
						matchWinner = match.team2;
					} else {
						matchWinner = null;
					}

					var guessedWinner;
					if (match.scorePronostic1 > match.scorePronostic2) {
						guessedWinner = match.team1;
					} else if (match.scorePronostic1 < match.scorePronostic2) {
						guessedWinner = match.team2;
					} else {
						guessedWinner = null;
					}

					return matchWinner === guessedWinner;
				} else {
					return false;
				}
			};

			var perfectScore = function(match, mIndex, contextId) {
				var matchPtr = prConf[contextId].matches[mIndex];
				if(!_.isUndefined(matchPtr)) {
					return (match.scorePronostic1 === matchPtr.score1 &&
						match.scorePronostic2 === matchPtr.score2);
				} else {
					return false;
				}
			}


			var getContextScore = function(context, cId) {

				var contextId = cId;
				if (!_.isUndefined(context.matches) && context.matches.length > 0) {
				context.matches = _.chain(context.matches)
				 .map(function(match, mIndex){
				 // Don't handle if user didn't answer because user can pronostic 
				 // a null score

				 	match.userScore = 0;
				 	var matchPtr = prConf[this.cId].matches[mIndex];

				 	if (!_.isUndefined(matchPtr)) {
					 	// Security checkpoints
					 	if (new Date(matchPtr.rawDate) > new Date() ||
					 		_.isUndefined(match.scorePronostic1) ||
					 		_.isUndefined(match.scorePronostic2) ||
					 		_.isNull(matchPtr.score1) ||
					 		_.isNull(matchPtr.score2)
					 		) {
					 		return match;
					 	}
				 	}



				 	if (winnerFound(match, mIndex, this.cId)) {
				 		match.userScore = config.pronostics.winnerScore;
				 		console.log('winner found');
				 	} else {
				 		match.userScore = 0;
				 	}

				 	if (perfectScore(match, mIndex, this.cId)) {
				 		match.userScore = config.pronostics.perfectScore;
				 		console.log('perfect score');
				 	}

				 	if (postPool()) {
				 		match.userScore *= 2;
				 		console.log('post pool score')
				 	}

				 	if (luckyDude(match, mIndex, this.cId)) {
				 		match.userScore *= 2;
				 		console.log('lucky dude score')
				 	}


				 	return match;

				 }, {context: context, cId: cId})
				 .value()	
				}
				

				// Context must be updated with score
				return context;
			}

			// For each user calculate score
			User.find({}, function(err, users){
				users.forEach(function(user, index) {
					if(!_.isUndefined(user.pronostics)) {
						var updatedPronostics = _.chain(user.pronostics)
						 .pick(['pool1', 'pool2', 'pool3', 'round16', 'quarter', 'semi', 'finale'])
						 .each(getContextScore)
						 .value()
						 // Update user pronostics with updated pronostics
						 user.update({pronostics: updatedPronostics, score: user.totalScore}, function(err, user){
						 	if (err) { res.json(400, err); }
						 });

					}
				});	
			})
			res.send(200);

		});
	})
}

exports.date = function(req, res) {
	res.json(Date.now());
};

exports.message = function(req, res) {
	res.send('Hello World');
};