'use strict';

var mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User');


/**
 * Logout
 */
exports.logout = function (req, res) {
  req.logout();
  res.send(200);
};


exports.login = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    var error = err || info;
    if (error) return res.json(401, error);
    req.logIn(user, function(err){
      if (err) return res.send(err);
      res.json(req.user);
    })
  })(req, res, next);

  // User.findOneAndUpdate({ facebookId: userfbId},
  //   req.body,
  //   { new: true, upsert: true},
  //   function(err, user){
  //     if (err) return next(err);
  //     res.send(user);
  //   })
}

/**
* Main Login
 */
exports.mainLogin = function (req, res, next) {
  // passport.authenticate('local', function(err, user, info) {
  //   var error = err || info;
  //   if (error) return res.json(401, error);

  //   req.logIn(user, function(err) {
      
  //     if (err) return res.send(err);
  //     res.json(req.user.userInfo);
  //   });
  // })(req, res, next);
  if ((req.body.email === 'highco' || req.body.email === 'Highco') && req.body.password === 'fancup2014*') {
    res.cookie('hubAuth', '1', { expires: new Date(Date.now() + 2592000 * 1000)});
    res.send(200);
  } else if ((req.body.email === 'admin') && req.body.password === 'admin42*@') {
    res.cookie('adminAuth', '1', { expires: new Date(Date.now() + 2592000 * 1000)});
    res.send(200);
  } else {
    res.send(403);
  }
};

exports.isLoggedIn = function(req, res) {
  if (req.isAuthenticated()) {
    res.send(req.user);
  } else {
    res.send(401);
  }
};

// fb authentication
exports.fbAuthenticate = function(req, res) {
  console.log('fb auth');
  passport.authenticate('facebook');
};

exports.fbCallback = function(req, res) {
  passport.authenticate('facebook', {
    successRedirect: '/',
    failureRedirect: '/login'
  });
};
