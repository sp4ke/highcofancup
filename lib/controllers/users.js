'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    _ = require('underscore'),
    passport = require('passport'),
    MATCH_CONTAINER_KEYS = ['pool1', 'pool2', 'pool3', 'quarter', 'round16', 'semi', 'finale'];

/**
 * Create user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.save(function(err) {
    if (err) return res.json(400, err);

    req.logIn(newUser, function(err) {
      if (err) return next(err);

      return res.json(req.user.userInfo);
    });
  });
};

/**
 * Activate user profile
 */
exports.activate = function(req, res, next) {

  var u = req.user;
  u.firstname = req.body.firstname;
  u.lastname = req.body.lastname;
  u.company = req.body.company;
  u.active = true;
  u.save(function(err){
    if (err) return next(err);
    res.send(req.user);
  });
};

/**
 * Update user pronostics
 */
exports.updatePronostics = function (req, res, next) {
  if (req.user.id !== req.body._id) {
    res.send({ error: 'User not corresponding' });
    return;
  }
  var
    u = req.user,
    pronostics = req.body.pronostics,
    now = Date.now(),

    paramsMatchContainer = null,
    userMatchContainer = null,
    userCurrentMatch = null;

  if (!u.pronostics || u.pronostics && Object.keys(u.pronostics).length === 0) {
    u.pronostics = new (mongoose.model('Pronostic'))();
  }

  MATCH_CONTAINER_KEYS.forEach(function (pronosticKey) {
    if (!u.pronostics[pronosticKey]) {
      u.pronostics[pronosticKey] = {
        name: null,
        matches: []
      };
    }
    userMatchContainer = u.pronostics[pronosticKey];
    paramsMatchContainer = pronostics[pronosticKey];

    userMatchContainer.name = paramsMatchContainer.name;
    if (!paramsMatchContainer.matches || paramsMatchContainer.matches instanceof Array && paramsMatchContainer.matches.length === 0) {
      for (var index = 0, len = userMatchContainer.matches.length; index < len; index++) {
        userMatchContainer.matches.push(paramsMatchContainer);
      }
    }

    if (paramsMatchContainer.matches instanceof Array && paramsMatchContainer.matches.length) {

      paramsMatchContainer.matches.forEach(function (paramsCurrentMatch) {
        var
          index = -1,
          len = userMatchContainer.matches.length,
          replaced = false,
          shouldPushNewOne = false,
          datetime = Number.MAX_VALUE;

          // update match pronostic only if the match date is over than now
          while (++index < len) {
            userCurrentMatch = userMatchContainer.matches[index];
            if (userCurrentMatch && userCurrentMatch.date === paramsCurrentMatch.date && userCurrentMatch.time === paramsCurrentMatch.time) {
              shouldPushNewOne = true;
              datetime = Date.parse(userCurrentMatch.date + ' ' + userCurrentMatch.time + ':00 GMT+0200');
              if (datetime - now > -120000) {
                userCurrentMatch.scorePronostic1 = paramsCurrentMatch.scorePronostic1;
                userCurrentMatch.scorePronostic2 = paramsCurrentMatch.scorePronostic2;
                replaced = true;
              }
              break;
            }
          }
          if (!replaced && !shouldPushNewOne) {
            userMatchContainer.matches.push(paramsCurrentMatch);
          }
      });

    }
  });
  u.markModified('pronostics');
  u.save(function (err, user) {
    if (err) {
      return next(err);
    }
    res.send(user);
  });
};

/**
 *  Get profile of specified user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.send(404);

    res.send({ profile: user.profile });
  });
};

/**
 * Change password
 */
exports.changePassword = function(req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if(user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function(err) {
        if (err) return res.send(400);

        res.send(200);
      });
    } else {
      res.send(403);
    }
  });
};


/**
 * Get all users
 */
exports.getAll = function(req, res, next) {
  User.find({}, function(err, users){
    if (err) return next(err);
    res.json(users);
  })
}

/**
 * Get current user
 */
exports.me = function(req, res) {
  if (req.user) {
  res.send(req.user.toJSON({virtuals: true}));
  } else {
    res.send(null);
  }
};