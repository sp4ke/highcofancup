'use strict';

var mongoose = require('mongoose'),
	Country = mongoose.model('Country'),
	path = require('path'),
	fs = require('fs'),
	config = require('./config');



var populateCountries = function() {
	var countriesJsonPath = path.join(__dirname, './countries.json');

	fs.readFile(countriesJsonPath, function(err, data){

		if (err) { console.dir(err); }

		var countriesData = JSON.parse(data);
		var worldCupCountries = [
		'DZA', 'CMR', 'CIV', 'GHA', 'NGA',
		 'AUS', 'IRN', 'JPN', 'KOR',
		 'BEL', 'BIH', 'HRV', 'GBR', 'FRA', 'DEU', 'GRC', 'ITA', 'NLD', 'PRT', 'RUS', 'ESP', 'CHE',
		 'CRI', 'HND', 'MEX', 'USA',
		 'ARG', 'BRA', 'CHL', 'COL', 'ECU', 'URY'

		];
		
		countriesData = countriesData.filter(function(country){
			return worldCupCountries.indexOf(country.cca3) !== -1;
		});
		
		Country.create(countriesData, function(err){
			if (err) { console.dir(err); }
			console.log('Populated countries');

		});
	});
};

Country.find({}).remove(function(){
	populateCountries();
});