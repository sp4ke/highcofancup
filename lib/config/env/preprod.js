'use strict';

module.exports = {
  env: 'preprod',
  ip:   process.env.OPENSHIFT_NODEJS_IP ||
        process.env.IP ||
        '0.0.0.0',
  serverURL: 'http://highcoworldcup.herokuapp.com',
  port: process.env.OPENSHIFT_NODEJS_PORT ||
        process.env.PORT ||
        8080,
  mongo: {
    uri: process.env.MONGOHQ_URL
  },
  facebook: {
    client: '690336737698356',
    secret: '03449c13a20a1f98791fb94c706de9d1'
  }
};