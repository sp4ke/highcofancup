'use strict';

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Thing = mongoose.model('Thing'),
  Country = mongoose.model('Country'),
  Pronostic = mongoose.model('Pronostic');

/**
 * Populate database with sample application data
 */

// Pronostic.find({}).remove(function(){
//   console.log('Pronostic cleared ');
//   // PronosticConf.create({
//   //   context: 'LES MATCHS DE GROUPE',
//   //   groups: [
//   //   {
//   //     name: 'Groupe A',
//   //     matches: [
//   //     {
//   //       date: '2014-06-28',
//   //       rawDate: '2014-06-28T00:02:00.000Z',
//   //       team1: {name: 'Brezil'},
//   //       team2: {name: 'Cameroun'},
//   //       time: '02:02'
//   //     },
//   //     {
//   //       date: '2014-06-28',
//   //       rawDate: '2014-06-28T00:02:00.000Z',
//   //       team1: {name: 'Croatie'},
//   //       team2: {name: 'Mexique'},
//   //       time: '02:02'
//   //     }
//   //     ]
//   //   },
//   //   {
//   //     name: 'Groupe B',
//   //     matches: [
//   //     {
//   //       date: '2014-06-28',
//   //       rawDate: '2014-06-28T00:02:00.000Z',
//   //       team1: {name: 'Espagne'},
//   //       team2: {name: 'Australie'},
//   //       time: '02:02'
//   //     },
//   //     {
//   //       date: '2014-06-28',
//   //       rawDate: '2014-06-28T00:02:00.000Z',
//   //       team1: {name: 'Pays Bas'},
//   //       team2: {name: 'Chili'},
//   //       time: '02:02'
//   //     }
//   //     ]
//   //   }
//   //   ]
//   // });
//     Pronostic.create({
//       contextTypes: {
//         pools: 'LES MATCHS DE GROUPE !',
//         round16: 'LES 8EME DE FINALE',
//         quarter: 'LES QUARTS DE FINALE',
//         semi: 'LES DEMI-FINALES',
//         finale: 'LA FINALE'
//       },
//       currentContext: 'pools',
//       currentPool: 'pool1',
//       pool1: {
//         name: 'Match 1',
//       },
//       pool2: {
//         name: 'Match 2'
//       },
//       pool3: {
//         name: 'Match 3'
//       },
//       round16: {
//         name: 'Les 8eme de finale'
//       },
//       quarter: {
//         name: 'LES QUARTS DE FINALE'
//       },
//       semi: {
//         name: 'LES DEMI-FINALES'
//       },
//       finale: {
//         name: 'LA FINALE'
//       }
//     });
// });

// //Clear old things, then add things in
// Thing.find({}).remove(function() {
//   Thing.create({
//     name : 'HTML5 Boilerplate',
//     info : 'HTML5 Boilerplate is a professional front-end template for building fast, robust, and adaptable web apps or sites.',
//     awesomeness: 10
//   }, {
//     name : 'AngularJS',
//     info : 'AngularJS is a toolset for building the framework most suited to your application development.',
//     awesomeness: 10
//   }, {
//     name : 'Karma',
//     info : 'Spectacular Test Runner for JavaScript.',
//     awesomeness: 10
//   }, {
//     name : 'Express',
//     info : 'Flexible and minimalist web application framework for node.js.',
//     awesomeness: 10
//   }, {
//     name : 'MongoDB + Mongoose',
//     info : 'An excellent document database. Combined with Mongoose to simplify adding validation and business logic.',
//     awesomeness: 10
//   }, function() {
//       console.log('finished populating things');
//     }
//   );
// });

// Clear old users, then add a default user
User.find({email: 'test@test'}).remove(function() {
  User.create({
    provider: 'local',
    firstname: 'Test',
    lastname: 'User',
    email: 'test@test',
    password: 'test'
  }, function() {
      console.log('finished populating users');
    }
  );
});
