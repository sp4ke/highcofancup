'use strict';

var mongoose = require('mongoose'),
    config = require('./config'),
    User = mongoose.model('User'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy;

/**
 * 
 * Passport configuration
 */
passport.serializeUser(function(user, done) {
  console.log('serialize()');
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  console.log('deserializeUser');
  User.findOne({
    _id: id
  }, '-salt -hashedPassword', function(err, user) { // don't ever give out the password or salt
    done(err, user);
  });
});

// add other strategies for more authentication flexibility
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'facebookId'
  },
  function(email, facebookId, done) {
    User.findOneAndUpdate({
      facebookId: facebookId
    }, { email: email, facebookId: facebookId},
     { new: true, upsert: true}, function(err, user) {
      if (err) return done(err);
      
      if (!user) {
        return done(null, false, {
          message: 'This email is not registered.'
        });
      }
      return done(null, user);
    });
  }
));

passport.use(new FacebookStrategy ({
  clientID: config.facebook.client,
  clientSecret: config.facebook.secret,
  callbackURL: config.serverURL + '/auth/facebook/callback/',
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOneAndUpdate({
     facebookId: profile.id 
    },
    {
      $setOnInsert: {
                      email: profile.emails[0].value,
                      facebookId: profile.id,
                      photos: profile.photos
                    }
    },
    {
      upsert: true,
      new: true
    }, function(err, user){
      if (err) return done(err);
      return done(null, user);
    });

    // done(null, profile);
    }
));

module.exports = passport;
