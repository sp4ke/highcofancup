'use strict';

var api = require('./controllers/api'),
    index = require('./controllers'),
    users = require('./controllers/users'),
    session = require('./controllers/session'),
    middleware = require('./middleware'),
    passport = require('passport');



/**
 * Application routes
 */
module.exports = function(app) {

  // Server API Routes
  
  app.route('/api/users')
    .post(users.create)
    .put(users.changePassword)
    .get(users.getAll);

  app.route('/api/users/me')
    .get(users.me);

  app.route('/api/users/:id')
    .get(users.show);

  app.route('/api/users/activate')
    .post(users.activate);

  app.route('/api/users/pronostics')
    .post(users.updatePronostics);


  // Authentication routes

  app.route('/api/session')
    .post(session.login)
    .delete(session.logout);

  app.route('/api/session/main')
    .post(session.mainLogin);

  app.get('/api/session/isLoggedIn', session.isLoggedIn);

  app.get('/api/time', api.date);
    

 
  app.get('/auth/facebook', passport.authenticate('facebook', {
    scope: 'email'
  }),
      function(req, res){
        //
      });

  app.get('/auth/facebook/callback', 
    passport.authenticate('facebook', {failureRedirect: '/pronostic/home'}),
    function(req, res){
      res.redirect('/pronostic/pronostics');
    });


  // end auth routes

  //////////////////
  // Logic API routes
  //////////////////

  // Pronostic Confs
  app.route('/api/pronostic')
    .post(api.savePronostic)
    .get(api.queryPronostic);

  app.route('/api/pronostic/score')
    .get(api.processScore);

  // countries
  app.route('/api/countries')
    .get(api.countries.get);



  // All undefined api routes should return a 404
  app.route('/api/*')
    .get(function(req, res) {
      res.send(404);
    });

  // All other routes to use Angular routing in app/scripts/app.js
  app.route('/partials/*')
    .get(index.partials);
  app.route('/*')
    .get( middleware.setUserCookie, index.index);
};