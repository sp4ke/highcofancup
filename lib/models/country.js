'user strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


/**
 * Country Schema
 */
var Country = new Schema({
	name: String,
	cca3: String,
	translations: Schema.Types.Mixed
});

mongoose.model('Country', Country);