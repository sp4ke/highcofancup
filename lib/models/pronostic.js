'user strict';

var mongoose = require('mongoose'),
	Country = mongoose.model('Country'),
	Schema = mongoose.Schema;


/*
 * Pronostic Schema
 */

 var MatchSchema = {
			team1: { type: Schema.Types.Mixed },
			team2: { type: Schema.Types.Mixed },
			rawDate: Date,
			date: String,
			time: String,
			score1: {type:Number},
			score2: {type: Number},
			scorePronostic1: {type:Number},
			scorePronostic2: {type: Number},
			userScore: {type: Number},
			group: String
 }

var Pronostic = new Schema({
	contextTypes: Schema.Types.Mixed,
	currentContext: String,
	currentPool: String,
	pool1: {
		name: String,
		matches:[MatchSchema]
	},
	pool2: {
		name: String,
		matches: [MatchSchema]
	},
	pool3: {
		name: String,
		matches: [MatchSchema]
	},
	round16: {
		name: String,
		matches: [MatchSchema]
	},
	quarter: {
		name: String,
		matches: [MatchSchema]
	},
	semi: {
		name: String,
		matches: [MatchSchema]
	},
	finale: {
		name: String,
		matches: [MatchSchema]
	}
});

/**
 * Validations
 */

mongoose.model('Pronostic', Pronostic);