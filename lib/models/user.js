'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    crypto = require('crypto');
  
var authTypes = ['github', 'twitter', 'facebook', 'google'];

/**
 * User Schema
 */
var UserSchema = new Schema({
  firstname: String,
  lastname: String,
  email: { type: String, lowercase: true },
  active: { type: Boolean, default: false},
  company: String,
  role: {
    type: String,
    default: 'user'
  },
  provider: String,
  salt: String,
  facebook: {},
  twitter: {},
  github: {},
  google: {},
  pronostics: {type: {}, default: {}},
  score: { type: Number, default: 0}
});


// Basic info to identify the current authenticated user in the app
UserSchema
  .virtual('userInfo')
  .get(function() {
    return {
      'name': this.name,
      'role': this.role,
      'provider': this.provider,
      'active': this.active,
      'pronostics': this.pronostics
    };
  });
  
UserSchema
  .virtual('name')
  .get(function(){
    return this.firstname + ' ' + this.lastname;
  });

// Public profile information
UserSchema
  .virtual('profile')
  .get(function() {
    return {
      'name': this.name,
      'role': this.role
    };
  });

UserSchema
  .virtual('totalScore')
  .get(function(){
    var p = this.pronostics;
    var totalScore = _.chain(p)
      .pick(['pool1', 'pool2', 'pool3', 'round16', 'quarter', 'semi', 'finale'])
      .map(function(context){

        var contextScore = _.chain(context.matches)
          .map(function(match){
            return match.userScore;
          })
          .reduce(function(memo, score){
            return memo + score;
          }, 0)
          .value()

        return contextScore;
      })
      .reduce(function(memo, contextScore){
        return memo + contextScore;
      }, 0)
      .value();

      return totalScore;
  });

    
/**
 * Validations
 */

// Validate empty email
UserSchema
  .path('email')
  .validate(function(email) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return email.length;
  }, 'Email cannot be blank');

// Validate email is not taken
UserSchema
  .path('email')
  .validate(function(value, respond) {
    var self = this;
    this.constructor.findOne({email: value}, function(err, user) {
      if(err) throw err;
      if(user) {
        if(self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
}, 'The specified email address is already in use.');

var validatePresenceOf = function(value) {
  return value && value.length;
};


/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function() {
    return crypto.randomBytes(16).toString('base64');
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function(password) {
    if (!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  },
};

module.exports = mongoose.model('User', UserSchema);
