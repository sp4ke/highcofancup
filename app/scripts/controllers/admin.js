'use strict';

angular.module('worldcupApp')
.controller('AdminCtrl', function ($scope, Pronostic, Countries, $location) {


	$scope.$watch(function(){
		return $location.path();
	}, function(val){
		if (val === '/admin') {
			$scope.$root.isAdmin = true;
		} else {
			$scope.$root.isAdmin = false;
		}
	});

	// Context list
	Pronostic.query(function(result){
		$scope.prConf = result[0];
	});

	$scope.countries = Countries.query();

	$scope.tmpContext = {
		groups: []
	};

	$scope.$watch(function(){
		if (angular.isDefined($scope.prConf)) {
			return $scope.prConf.currentContext;
		}
	}, function(val) {
		if (angular.isDefined(val)) {
			if (val !== 'pools'){
				$scope.contextPtr = val;
			} else {
				$scope.contextPtr = $scope.prConf.currentPool;
			}
		}
	});

	$scope.$watch(function(){
		if (angular.isDefined($scope.prConf)){
			return $scope.prConf.currentPool;
		}
	}, function(val) {
		if (angular.isDefined(val)) {
			if ($scope.prConf.currentContext === 'pools') {
				$scope.contextPtr = $scope.prConf.currentPool;
			} else {
				$scope.contextPtr = $scope.prConf.currentContext;
			}
		}
	});


	/**
	 * Add new pronostic conf
	 */
	$scope.saveConf = function() {
		$scope.prConf.$save(function(){
			$scope.saveMsg = 'Configuration des pronostics mise à jour avec succés.';
			$scope.alert = 'success';
		}, function(){
			$scope.saveMsg = 'Probleme au niveau de la sauvegarde !';
			$scope.alert = 'error';
		});
	};

	$scope.updateMatchDate = function(match) {
		match.rawDate = new Date(match.date);
		match.rawDate.setHours(match.time.split(':')[0]);
		match.rawDate.setMinutes(match.time.split(':')[1]);
	};

	$scope.isCurrentConf = function(conf) {
		return conf === $scope.currentConf;
	};

	$scope.addGroup = function(groupName) {
		$scope.prConf.groups.push({
			name: groupName,
			matches: []
		});
		$scope.showAddGroup = false;
	};

	$scope.removeGroup = function(group) {
		$scope.prConf.groups = _.without($scope.prConf.groups, group);
	};

	$scope.addMatch = function(match) {
		match.rawDate = new Date(match.date);
		match.rawDate.setHours(match.time.split(':')[0]);
		match.rawDate.setMinutes(match.time.split(':')[1]);
		$scope.prConf[$scope.contextPtr].matches.push(angular.copy(match));
		match = {};
	};

	$scope.removeMatch = function(group, match) {
		$scope.prConf[$scope.contextPtr].matches = _.without($scope.prConf[$scope.contextPtr].matches, match);
	};

	$scope.calculateScore = function() {
		$scope.prConf.$save(function(){
			Pronostic.score();
		}, function(err){
			console.log('Error on pronostic score ' + err);
		});
	};

});
