'use strict';

angular.module('worldcupApp')
.controller('NavbarCtrl', function ($scope, $location, Auth, $route) {
	
	$scope.logout = function() {
		Auth.logout()
		.then(function() {
			$location.path('/login');
		});
	};
	
	$scope.isActive = function(route) {
		return route === $location.path();
	};

	$scope.isPronostic = function(){


		var isPronoPath = $location.path().split('/')[1];
		if (angular.isDefined($route.current)) {
			return $route.current.isPronostic && isPronoPath === 'pronostic';
		}
		// if (angular.isDefined($scope.$root.currentUser) && !_.isNull($scope.$root.currentUser)) {
		// 	return $route.current.isPronostic;
		// }
	};

});
