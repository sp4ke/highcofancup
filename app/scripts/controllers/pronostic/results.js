'use strict';

angular.module('worldcupApp')
  .controller('PronosticResultsCtrl', function ($scope, $http, Pronostic, User, $filter) {

    $scope.user = User.get();

  	Pronostic.query(function(pronostics){
  		$scope.prConf = pronostics[0];
	  	$scope.contextPtr = $scope.prConf[$scope.currentContext];
  	});

    $scope.allUsers = User.query(function(users){
      var orderBy = $filter('orderBy');
      var sorted = orderBy(users, '-score');
      $scope.currentUser.ranking = _.chain(sorted)
      .map(function(user){ return user._id; })
      .indexOf($scope.currentUser._id)
      .value() + 1;
    });

  	$scope.currentContext = 'pool1';

  	$scope.showPronostic = function(match, $index) {
  		if (angular.isDefined($scope.prConf)) {
  			return new Date($scope.prConf[$scope.currentContext].matches[$index].rawDate) < new Date(); 
  		}
  	};

  	$scope.$watch('currentContext', function(val){
  		if (angular.isDefined(val) && 
  			angular.isDefined($scope.prConf)) {
  			$scope.contextPtr = $scope.prConf[val];
  		}
  	});

  });
