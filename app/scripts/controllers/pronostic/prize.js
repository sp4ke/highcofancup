'use strict';

angular.module('worldcupApp')
  .controller('PronosticPrizeCtrl', function ($scope, $http, $translate) {
  	$scope.curLang = $translate.use();
    $http.get('/api/awesomeThings').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
    });
  });
