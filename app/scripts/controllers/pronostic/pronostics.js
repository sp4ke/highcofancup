'use strict';

angular.module('worldcupApp')
  .controller('PronosticPronosticsCtrl', ['$scope', 'Pronostic', 'Countries', 'User', '$q', '$translate', '$interval', '$timeout', function ($scope, Pronostic, Countries, User, $q, $translate, $interval, $timeout) {

    $scope.$translate = $translate;


    var setMatchesByGroup = function() {
      $scope.matchesByGroup = _.groupBy($scope.prConf[$scope.contextPtr].matches, function(match) {
        return match.group;
      });
      if (!$scope.$digest) {
        $scope.$apply();
      }
    };


   
    $scope.currentUser = User.get();

  	Pronostic.query(function(result){
      $scope.prConf = result[0];
      // We need to pick up user filled score !

      // $scope.tmpPrConf = _.extend($scope.prConf,  $scope.currentUser.pronostics);
      // $scope.prConf  = _.extend($scope.tmpPrConf, $scope.prConf);

      var t = _.chain($scope.currentUser.pronostics)
        .map(function(val, key){
          if(!_.isUndefined(val.matches)) {
            return {cId: key, context: val};
          }
         })
        .without(undefined)
        .filter(function(pair){
          return (pair.context.matches.length > 0);
        })
        .each(function(pair){
          _.each(pair.context.matches, function(match, mIndex){
            if (!_.isUndefined(match.scorePronostic1) && !_.isUndefined(match.scorePronostic2)) {
              if (new Date($scope.prConf[this].matches[mIndex].rawDate) > $scope.$root.globalTime) {
                $scope.prConf[this].matches[mIndex].scorePronostic1 = match.scorePronostic1;
                $scope.prConf[this].matches[mIndex].scorePronostic2 = match.scorePronostic2;
              };
            }
          }, pair.cId);
        });

      if ($scope.prConf.currentContext === 'pools') {
       $scope.contextPtr = 'pool1';
     }
     $scope.countries = Countries.query();
   });


  	$scope.$watch(function(){
  		if (angular.isDefined($scope.prConf)) {
  			return $scope.prConf.currentContext;
      }
  	}, function(val) {
  		if (angular.isDefined(val)) {
  			if (val !== 'pools') {
  				$scope.contextPtr = val;
        }
  		}
  	});

  	$scope.$watch(function(){
  		if (angular.isDefined($scope.prConf)) {
  			return $scope.currentPool;
      }
  	}, function(val) {
  		if (angular.isDefined(val)) {
  			if ($scope.prConf.currentContext === 'pools') {
  				$scope.contextPtr = $scope.currentPool;
          setMatchesByGroup();
  			} else {
  				$scope.contextPtr = $scope.prConf.currentContext;
  			}
  		}
  	});

    $scope.$watch('prConf', function(val){
      if (angular.isDefined(val)){ 
        setMatchesByGroup();
      }
    });


    // Watch time and update field status
    $interval(function() {
      if (!$scope.$digest) {
        $scope.$apply();
      }
    }, 1000);

    $scope.submitPronositc = function() {
      if (new Date() >= $scope.$root.globalTime) {
        $scope.currentUser.pronostics = $scope.prConf;
        $scope.currentUser.$pronostic(function(user){
          console.log('Pronostics updated');
          $scope.showMessage = true;
          $scope.message = 'SUCCESS';
        }, function(err){
          console.log('Error on pronostic update');
          $scope.showMessage = true;
          $scope.message = 'ERROR';
        });
      } else {
        $scope.showMessage = true;
        $scope.message = 'TIME SYNCHRONISATION ERROR';
      }
    };

    $scope.disablePronostic = function(match, $index) {
      var matchIndex = $scope.prConf[$scope.contextPtr].matches.indexOf(match);
      if (angular.isDefined($scope.prConf)) {
        return new Date($scope.prConf[$scope.contextPtr].matches[matchIndex].rawDate) < $scope.$root.globalTime; 
      }
    };

  }]);
