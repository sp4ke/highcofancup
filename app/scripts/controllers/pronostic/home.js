"use strict";

angular.module('worldcupApp')
  .controller('PronosticHomeCtrl', function ($scope, $http, Auth, $translate, $facebook, $location) {
  	$scope.curLang = $translate.use();
  	if (angular.isDefined($scope.currentUser) || Auth.isLoggedIn()) {
  		$scope.loggedIn = true;
  	}


  	$scope.fbAndLogin = function() {
  		$facebook.getLoginStatus()
  		.then(function(result){
  			if (result.status !== 'connected') {
  				$facebook.login()
  				.then(function(result){
  					$facebook.cachedApi('/me')
  					.then(function(result){
  						Auth.login(result, function(){
	  						$location.path('/pronostic/pronostics');
  						});
  					})

  				})
  			} else {

  				// make sure user is logged in to backend
  				$facebook.cachedApi('/me')
  				.then(function(result){
  					Auth.login(result, function(){
  						$location.path('/pronostic/pronostics');
  					})
  				})
  			}
  		})
  	};

  });
