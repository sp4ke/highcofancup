'use strict';

angular.module('worldcupApp')
  .controller('PronosticRankingCtrl', function ($scope, $http, User, $filter) {

  	$scope.user = User.get();
  	$scope.allUsers = User.query(function(users){
  		var orderBy = $filter('orderBy');
  		var sorted = orderBy(users, '-score');
  		$scope.currentUser.ranking = _.chain(sorted)
		.map(function(user){ return user._id; })
		.indexOf($scope.currentUser._id)
		.value() + 1;
  	});

  });
