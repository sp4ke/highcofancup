'use strict';

angular.module('worldcupApp')
.controller('MainCtrl', function ($scope, $translate) {
	$scope.curLang = $translate.use();
});
