'use strict';

angular.module('worldcupApp')
  .controller('LoginCtrl', function ($scope, Auth, $location, $timeout, $http) {
    $scope.user = {};
    $scope.errors = {};

    $scope.login = function(form) {
      $scope.submitted = true;
      
      if(form.$valid) {
        $http.post('/api/session/main', {
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function(response) {
          // Logged in, redirect to home
          // console.log('redirect');
          $timeout(function(){
            $location.path('/');
          }, 500);
        })
        .catch( function(err) {
          err = err.data;
          $scope.errors.other = 'PASSWORD_ERROR';
        });
      }
    };


    $scope.fbLogin = function() {
      Auth.fbLogin();
    };

  });