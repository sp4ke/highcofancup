"use strict";

angular.module('worldcupApp')
.controller('ActivateCtrl',['$scope', '$http', 'User', '$route', '$location', function ($scope, $http, User, $route, $location) {
	$scope.currentUser = User.get();

	$scope.activateUser = function() {
		$scope.currentUser.active = true;
		$scope.currentUser.$activate(function(user){
			if (user.active) {
				$scope.$root.currentUser = user;
				$location.path($route.current.next);
			}
		});
	};

}]);
