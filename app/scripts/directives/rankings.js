"use strict";

angular.module('worldcupApp')
.directive('rankings', function ($timeout) {
	return {
		restrict: 'EA',
		link: function postLink(scope, element, attrs) {
		 element.perfectScrollbar({suppressScrollX: true});
		 $timeout(function(){
			element.trigger('mousewheel');
		}, 100);
	 }
 };
});
