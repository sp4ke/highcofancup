'use strict';

angular.module('worldcupApp')
  .factory('User', function ($resource) {
    return $resource('/api/users/:id', {
      id: '@id'
    }, { //parameters default
      update: {
        method: 'PUT',
        params: {}
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      },
      activate: {
        method: 'POST',
        params: {
          id: 'activate'
        }
      },
      pronostic: {
        method: 'POST',
        params: {
          id: 'pronostics'
        }
      }
	  });
  });
