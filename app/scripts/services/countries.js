'use strict';

angular.module('worldcupApp')
  .factory('Countries', function Countries($resource) {
    return $resource('/api/countries/:id');
  });
