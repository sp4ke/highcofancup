'use strict';

angular.module('worldcupApp')
  .factory('Pronostic', function Pronostic($resource) {
	return $resource('/api/pronostic/:id', {
		id: '@id'
	}, {
		score: {
			method: 'GET',
			params: {
				id: 'score'
			}
		}
	});
});
