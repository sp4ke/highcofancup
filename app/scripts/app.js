'use strict';

var mainAuth = ['$cookies', '$location', function($cookies, $location) {
	if (!angular.isDefined($cookies.hubAuth) || $cookies.hubAuth !== '1') {
		$location.path('/login');
	}
}];

var adminAuth = ['$cookies', '$location', function($cookies, $location) {
	if (!angular.isDefined($cookies.hubAuth) || $cookies.adminAuth !== '1') {
		$location.path('/login');
	}
}]

angular.module('worldcupApp', [
	'ngCookies',
	'ngResource',
	'ngSanitize',
	'ngRoute',
	'pascalprecht.translate',
	'ngFacebook'
])
	.config(function ($routeProvider, $locationProvider, $httpProvider, $translateProvider, $facebookProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'partials/main',
				controller: 'MainCtrl',
				resolve: {
					auth: mainAuth
				}
			})

			.when('/login', {
				templateUrl: 'partials/login',
				controller: 'LoginCtrl',
			})
			.when('/signup', {
				templateUrl: 'partials/signup',
				controller: 'SignupCtrl'
			})
			.when('/settings', {
				templateUrl: 'partials/settings',
				controller: 'SettingsCtrl',
				authenticate: true
			})

	 
			.when('/pronostic/home', {
				templateUrl: 'partials/pronostic/home',
				controller: 'PronosticHomeCtrl',
				isPronostic: false,
				resolve: {
					user: ['Auth', '$location', function(Auth, $location){
						if (Auth.isLoggedIn()) {
							$location.path('/pronostic/pronostics');
						}
					}],
					auth: mainAuth
				}
			})

		.when('/pronostic/pronostics', {
				templateUrl: 'partials/pronostic/pronostics',
				controller: 'PronosticPronosticsCtrl',
				isPronostic: true,
				resolve: {
					userActive: ['$rootScope', '$location', 'Auth', '$q', function($rootScope, $location, Auth, $q) {

						Auth.currentUser().$promise.then(function(user){
							if (!user.active) {
								$location.path('/activate');
							}
						});

						
					}],
					auth: mainAuth
				}
			})

			.when('/pronostic/results', {
				templateUrl: 'partials/pronostic/results',
				controller: 'PronosticResultsCtrl',
				isPronostic: true,
				authenticate: true,
				resolve: {
					auth: mainAuth
				}
			})
			.when('/pronostic/ranking', {
				templateUrl: 'partials/pronostic/ranking',
				controller: 'PronosticRankingCtrl',
				isPronostic: true,
				authenticate: true,
				resolve: {
					auth: mainAuth
				}
			})

			.when('/pronostic/prize', {
				templateUrl: 'partials/pronostic/prize',
				controller: 'PronosticPrizeCtrl',
				isPronostic: true,
				authenticate: true,
				resolve: {
					auth: mainAuth
				}
			})

			.when('/activate', {
				templateUrl: 'partials/activate',
				controller: 'ActivateCtrl',
				authenticate: true,
				isPronostic: true,
				next: '/pronostic/pronostics',
				resolve: {
					auth: mainAuth
				}
			})

			// Admin backoffice 
			.when('/admin', {
				templateUrl: 'partials/admin',
				controller: 'AdminCtrl',
				authenticateAdmin: true,
				resolve: {
					auth: adminAuth
				}
			})
			
		
			.otherwise({
				redirectTo: '/'
			});


		$facebookProvider.setAppId('690336737698356');
		$facebookProvider.setPermissions('email');

		$translateProvider.useStaticFilesLoader({
			prefix: '/languages/',
			suffix: '.json'
		});

		$translateProvider.determinePreferredLanguage(function(){
			if (angular.isDefined(window.navigator.language)) {
				return window.navigator.language.split('-')[0];
			} else {
				return window.navigator.userLanguage.split('-')[0];
			}
		});
			
		$locationProvider.html5Mode(true);
		// $httpProvider.defaults.withCredentials = true;
		// delete $httpProvider.defaults.headers.common['X-Requested-With'];

		// Intercept 401s and redirect you to login
		$httpProvider.interceptors.push(['$q', '$location', '$cookies', function($q, $location, $cookies) {
			return {
				'responseError': function(response) {
					if(response.status === 401) {
						if (angular.isDefined($cookies.hubAuth) && $cookies.hubAuth === '1') {
							$location.path('/pronostic/home');
						}
						return $q.reject(response);
					}
					else {
						return $q.reject(response);
					}
				}
			};
		}]);

	})



	.run(function ($rootScope, $location, Auth, $http, $cookies, $translate, $facebook, $interval) {


		(function(){
		     // If we've already installed the SDK, we're done
		     if (document.getElementById('facebook-jssdk')) {return;}

		     // Get the first script element, which we'll use to find the parent node
		     var firstScriptElement = document.getElementsByTagName('script')[0];

		     // Create a new script element and set its id
		     var facebookJS = document.createElement('script'); 
		     facebookJS.id = 'facebook-jssdk';

		     // Set the new script's source to the source of the Facebook JS SDK
		     facebookJS.src = '//connect.facebook.net/en_US/all.js';

		     // Insert the Facebook JS SDK into the DOM
		     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
		   }());

		$rootScope.curLang = $translate.use();

		// Redirect to login if route requires auth and you're not logged in
		$rootScope.$on('$routeChangeStart', function (event, next) {

			
			if (next.authenticate) {
				$http.get('/api/session/isLoggedIn')
				.then(function(result) {
					$rootScope.currentUser = result.data;
				}, function(){
					$http.delete('/api/session').then(function(){
						if (angular.isDefined($cookies.hubAuth) && $cookies.hubAuth === '1') {
							$location.path('/pronostic/home');
						}
					})
				});
			}

		});

		$rootScope.$on('$routeChangeSuccess', function(event, current) {
			if (angular.isDefined(current.isPronostic)) {
				$rootScope.isPronostic = true;
			} else {
				$rootScope.isPronostic = false;
			}
		});


		$http.get('/api/time')
		.then(function(result){
			$rootScope.globalTime = new Date(parseInt(result.data));
		});

		
		$interval(function(){
			$http.get('/api/time')
			.then(function(result){
				$rootScope.globalTime = new Date(parseInt(result.data));
			})
		}, 10000)

	});