"use strict";

angular.module('worldcupApp')
.filter('score', function () {
  return function (input) {
    if (input > 0) {
     return '+' + input;
   } else if (input < 0) {
     return '-' + input;
   } else {
     return input;
   }
 };
});
