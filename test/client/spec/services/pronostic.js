'use strict';

describe('Service: Pronostic', function () {

  // load the service's module
  beforeEach(module('worldcupApp'));

  // instantiate service
  var Pronostic;
  beforeEach(inject(function (_Pronostic_) {
    Pronostic = _Pronostic_;
  }));

  it('should do something', function () {
    expect(!!Pronostic).toBe(true);
  });

});
